# Correr el app
### Run the app 
```
docker-compose -f file.yml up
```

### Entrar al shell 
```
docker exec -it containerName mongo  
  or
docker exec -i containerName mongo  
```

# Docker comandos 

### Show all containers
```
docker ps
  or
docker ps -a  
```
### Docker show all images
```
docker images 
```

### Stop all containers
```
docker stop $(docker ps -a -q)
```

### Delete all the containers
```
docker rm $(docker ps -a -q)
```

### Delete all images
```
docker rmi $(docker images -q)
```